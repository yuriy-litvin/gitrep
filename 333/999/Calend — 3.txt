[Продажи]:
LOAD 
     Дата, 
     [Страна ID], 
     Продажи, 
     Скидка, 
     [Модель ID]
FROM [Продажи 2010-2021.qvd] (qvd);



//
//[Календарь]:
//LOAD
//	[Дата],
//	NUM(FLOOR([Дата]))													as [DateIndex],
//	DAY([Дата])                     									as [День],
//	WEEK([Дата])  		          										as [Неделя],	
//	Year([Дата])                    									as [Год],			
//	weekday([Дата])														as [День недели],
//	WeekName([Дата])													as [Неделя Имя],
//	NUM(weekday([Дата]))												as [ДеньНедели Номер],
//	Date(MonthStart([Дата]), 'MMM-YYYY') 								as [Месяц_Год],
//	
//	WeekYear([Дата])&
//	   'W'&NUM(Week([Дата]),'00')										as [Неделя_Год],
//	
//	Year([Дата])&''&
//	     'Q'&Ceil(month([Дата])/3)										as [ГодКвартал],
//	
//	'Q'&Ceil(month([Дата])/3)											as [Квартал],     
//
//	Month([Дата])                                						as [Месяц];
//
//LOAD
//	FieldValue('Дата', IterNo()) as [Дата]
//		AutoGenerate(1)
//	While not IsNull(FieldValue('Дата',IterNo()));



  
    tmpDate:
    LOAD 
    	 MIN([Дата]) 													as tmpMinDate, 
    	 MAX([Дата]) 													as tmpMaxDate 
    RESIDENT [Продажи];
   
    LET varStartDate = PEEK('tmpMinDate', 0, 'tmpDate');
    LET varEndDate = PEEK('tmpMaxDate', 0, 'tmpDate');
   
    LET varNoOfDays = varEndDate-varStartDate+1;
    DROP TABLE tmpDate;
   
//////////////////////////////////////////////////////////////////////////////////////////
////////       Создадим Календарь с массивом дат, 
[Календарь2]:
LOAD
//	DATE([Дата])														as [Дата],
	DATE([Дата])														as [Дата],
	NUM(FLOOR([Дата]))													as [DateIndex],
	DAY([Дата])                     									as [День],
	WEEK([Дата])  		          										as [Неделя],	
	Year([Дата])                    									as [Год],			
	weekday([Дата])														as [День недели],
	WeekName([Дата])													as [Неделя Имя],
	NUM(weekday([Дата]))												as [ДеньНедели Номер],
	Date(MonthStart([Дата]), 'MMM-YYYY') 								as [Месяц_Год],
	
	WeekYear([Дата])&
	   'W'&NUM(Week([Дата]),'00')										as [Неделя_Год],
	
	Year([Дата])&''&
	     'Q'&Ceil(month([Дата])/3)										as [ГодКвартал],
	
	'Q'&Ceil(month([Дата])/3)											as [Квартал],     

	Month([Дата])                                						as [Месяц];
LOAD
         ($(varStartDate)+RecNo()-1) 									as Дата
    AUTOGENERATE $(varNoOfDays);
